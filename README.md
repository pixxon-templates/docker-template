# release-template

This project contains all the necessary features that are required to get a project going with Gitlab environments and docker images.

## jobs

### build_image

During merge requests, the job validates the source code by creating a docker image specified by the `Dockerfile`. It allows the project to use different technologies and programming languages, all of that will be abstracted behind the docker build step. This step also uploads the build image under the name of the short sha of the commit it was built from.

### docker_cli_setup

A base job that will setup the docker cli to interact with a remote docker engine specified via variables. It uses an `SSH_PRIVATE_KEY` to authentication towards the `DOCKER_HOST`. `DOCKER_HOSTNAME` is used make the host known to the system. The environments will be deployed on this remote system and can be accessed from there.

### deploy_review

Uses the `docker-compose.review.yaml` to create a docker stack on the remote host. These stacks are dynamic environments prepared to perform review on the implemented features and fixes in the merge requests. Once the merge request is closed, the environment is deleted automatically.

### stop_review

Performs the cleanup from the review environment. First deletes the stack from the remote host, then deletes the review image from the docker registry.

### deploy_staging

Similar to `deploy_review` but runs everytime there is a new change on the default branch. Allows the current `HEAD` of the code to be available for testing.

### stop_staging

Stops the staging environment and cleans up the docker image.

### deploy_production

Similar to `deploy_review` but runs everytime there is a new tag created. Creates the production environment which is considered the live product available to end users.

### stop_production

Stops the production environment, unlike other cleanups, does not delete the docker image.

## project settings

### ci/cd

The following variables must be set to allow the pipeline to function properly:
- **DOCKER_HOST**: the way docker cli can access the docker engine. Must be accessible in protected envrionments too.
- **DOCKER_HOSTNAME**: the name of the host under which both the pipeline and the outside can access the remote.
- **SSH_PRIVATE_KEY**: private ssh key which is used for authentication to the remote, it has to be base64 encoded.
